FROM node:lts-alpine

RUN apk add git

WORKDIR /app

COPY package*.json ./

RUN npm install 

COPY . .

EXPOSE 3001

CMD ["node", "server"]