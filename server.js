var path = require('path');
var express = require('express');
var ws = require('ws');
var kurento = require('kurento-client');
var https = require('https');
var fs = require('fs');

var ws_uri = 'ws://3.21.186.222:8888/kurento';

var candidatesQueue = {};
var kurentoClient = null;
var presenters = [];
var noPresenterMessage = 'No active presenter. Try again later...';

var app = express();

var options =
{
	key: fs.readFileSync('keys/server.key'),
	cert: fs.readFileSync('keys/server.crt')
};


/*
 * Server startup
 */
var server = https.createServer(options, app).listen("3001", function() {
    console.log('Kurento started');
});

var wss = new ws.Server({
    server : server,
    path : '/one2many'
});

/*
 * Management of WebSocket messages
 */
wss.on('connection', function(ws) {

	var sessionId = null;
	var eventId = null;

    ws.on('error', function(error) {
        console.log('Connection sessionId:' + sessionId + ' | eventId: '+ eventId + ' error');
        stop(eventId, sessionId);
    });

    ws.on('close', function() {
        console.log('Connection sessionId:' + sessionId + ' | eventId: '+ eventId + ' closed');
        stop(eventId, sessionId);
    });

    ws.on('message', function(_message) {
        var message = JSON.parse(_message);
        switch (message.id) {
		case 'sendIds':
			sessionId = message.userId;
			eventId = message.eventId;
			break;

        case 'presenter':
			startPresenter(eventId, sessionId, ws, message.sdpOffer, function(error, sdpAnswer) {
				if (error) {
					return ws.send(JSON.stringify({
						id : 'presenterResponse',
						response : 'rejected',
						message : error
					}));
				}
				ws.send(JSON.stringify({
					id : 'presenterResponse',
					response : 'accepted',
					sdpAnswer : sdpAnswer
				}));
			});
			break;

        case 'viewer':
			startViewer(eventId, sessionId, ws, message.sdpOffer, function(error, sdpAnswer) {
				if (error) {
					return ws.send(JSON.stringify({
						id : 'viewerResponse',
						response : 'rejected',
						message : error
					}));
					removeViewersCount(eventId);
				}
				ws.send(JSON.stringify({
					id : 'viewerResponse',
					response : 'accepted',
					sdpAnswer : sdpAnswer
				}));
				addViewersCount(eventId);
			});
			break;

        case 'stop':
            stop(eventId, sessionId);
            break;

        case 'onIceCandidate':
            onIceCandidate(message.type, eventId, sessionId, message.candidate);
            break;

        default:
            ws.send(JSON.stringify({
                id : 'error',
                message : 'Invalid message ' + message
            }));
            break;
        }
    });
});

/*
 * Definition of functions
 */

// Recover kurentoClient for the first time.
function getKurentoClient(callback) {
    if (kurentoClient !== null) {
        return callback(null, kurentoClient);
    }

    kurento(ws_uri, function(error, _kurentoClient) {
        if (error) {
            console.log("Could not find media server at address " + ws_uri);
            return callback("Could not find media server at address" + ws_uri
                    + ". Exiting with error " + error);
        }

        kurentoClient = _kurentoClient;
        callback(null, kurentoClient);
    });
}

function startPresenter(eventId, sessionId, ws, sdpOffer, callback) {
	
	clearCandidatesQueue(sessionId);

	presenters[eventId] = {
		id : sessionId,
		pipeline : null,
		webRtcEndpoint : null,
		ws : ws,
		eventViewers: []
	}

	getKurentoClient(function(error, kurentoClient) {
		if (error) {
			stop(eventId, sessionId);
			return callback(error);
		}

		if (presenters[eventId] === null || typeof presenters[eventId] === 'undefined') {
			stop(eventId, sessionId);
			return callback(noPresenterMessage);
		}

		kurentoClient.create('MediaPipeline', function(error, pipeline) {
			if (error) {
				stop(eventId, sessionId);
				return callback(error);
			}

			if (presenters[eventId] === null || typeof presenters[eventId] === 'undefined') {
				stop(eventId, sessionId);
				return callback(noPresenterMessage);
			}

			presenters[eventId].pipeline = pipeline;
			pipeline.create('WebRtcEndpoint', function(error, webRtcEndpoint) {
				if (error) {
					stop(eventId, sessionId);
					return callback(error);
				}

				if (presenters[eventId] === null || typeof presenters[eventId] === 'undefined') {
					stop(eventId, sessionId);
					return callback(noPresenterMessage);
				}

				presenters[eventId].webRtcEndpoint = webRtcEndpoint;

                if (candidatesQueue[sessionId]) {
                    while(candidatesQueue[sessionId].length) {
                        var candidate = candidatesQueue[sessionId].shift();
                        webRtcEndpoint.addIceCandidate(candidate);
                    }
                }

                webRtcEndpoint.on('OnIceCandidate', function(event) {
                    var candidate = kurento.getComplexType('IceCandidate')(event.candidate);
                    ws.send(JSON.stringify({
                        id : 'iceCandidate',
                        candidate : candidate
                    }));
                });

				webRtcEndpoint.processOffer(sdpOffer, function(error, sdpAnswer) {
					if (error) {
						stop(eventId, sessionId);
						return callback(error);
					}

					if (presenters[eventId] === null || typeof presenters[eventId] === 'undefined') {
						stop(eventId, sessionId);
						return callback(noPresenterMessage);
					}

					callback(null, sdpAnswer);
				});

                webRtcEndpoint.gatherCandidates(function(error) {
                    if (error) {
                        stop(eventId, sessionId);
                        return callback(error);
                    }
                });
            });
        });
	});
}

function startViewer(eventId, sessionId, ws, sdpOffer, callback) {
	clearCandidatesQueue(sessionId);

	if (presenters[eventId] === null || typeof presenters[eventId] === 'undefined') {
		stop(eventId, sessionId);
		return callback(noPresenterMessage);
	}

	presenters[eventId].pipeline.create('WebRtcEndpoint', function(error, webRtcEndpoint) {
		if (error) {
			stop(eventId, sessionId);
			return callback(error);
		}

		if (presenters[eventId] === null || typeof presenters[eventId] === 'undefined') {
			stop(eventId, sessionId);
			return callback(noPresenterMessage);
		}

		if (candidatesQueue[sessionId]) {
			while(candidatesQueue[sessionId].length) {
				var candidate = candidatesQueue[sessionId].shift();
				webRtcEndpoint.addIceCandidate(candidate);
			}
		}

        webRtcEndpoint.on('OnIceCandidate', function(event) {
            var candidate = kurento.getComplexType('IceCandidate')(event.candidate);
            ws.send(JSON.stringify({
                id : 'iceCandidate',
                candidate : candidate
            }));
        });

		webRtcEndpoint.processOffer(sdpOffer, function(error, sdpAnswer) {
			if (error) {
				stop(eventId, sessionId);
				return callback(error);
			}
			if (presenters[eventId] === null || typeof presenters[eventId] === 'undefined') {
				stop(eventId, sessionId);
				return callback(noPresenterMessage);
			}

			presenters[eventId].webRtcEndpoint.connect(webRtcEndpoint, function(error) {
				
				if (error) {
					stop(eventId, sessionId);
					return callback(error);
				}

				if (presenters[eventId] === null || typeof presenters[eventId] === 'undefined') {
					stop(eventId, sessionId);
					return callback(noPresenterMessage);
				}

				presenters[eventId].eventViewers.push({
					sessionId: sessionId,
					webRtcEndpoint : webRtcEndpoint,
					ws : ws
				});

				callback(null, sdpAnswer);
		        webRtcEndpoint.gatherCandidates(function(error) {
		            if (error) {
			            stop(eventId, sessionId);
			            return callback(error);
		            }
		        });
		    });
	    });
	});
}

function clearCandidatesQueue(sessionId) {
	if (candidatesQueue[sessionId]) {
		delete candidatesQueue[sessionId];
	}
}

function stop(eventId, sessionId) {
	if (typeof presenters[eventId] !== 'undefined' && presenters[eventId]?.id == sessionId) {
		for (var i in presenters[eventId]?.eventViewers) {
			var viewer = presenters[eventId]?.eventViewers[i];
			if (viewer.ws) {
				viewer.ws.send(JSON.stringify({
					id : 'stopCommunication'
				}));
			}
		}
		presenters[eventId].pipeline.release();
		if(presenters[eventId].ws){
			presenters[eventId].ws.send(JSON.stringify({
				id : 'stopCommunication'
			}));
		}
		presenters[eventId] = null;
	}else if (presenters[eventId]?.eventViewers?.length > 0) {
		for (var i in presenters[eventId]?.eventViewers) {
			var viewer = presenters[eventId]?.eventViewers[i];
			if (viewer.ws && viewer.sessionId == sessionId) {
				viewer.ws.send(JSON.stringify({
					id : 'stopCommunication'
				}));
				presenters[eventId]?.eventViewers.splice(i, 1);
			}
		}
	}
	removeViewersCount(eventId);
	console.log('Connection closed: sid=' + sessionId + ' eventId=' + eventId);
	clearCandidatesQueue(sessionId);
}

function onIceCandidate(type, eventId, sessionId, _candidate) {
    var candidate = kurento.getComplexType('IceCandidate')(_candidate);

    if (type == "presenter" && presenters[eventId].webRtcEndpoint) {
        console.info('Sending presenter candidate: sid=' + sessionId + ' eventId=' + eventId);
        presenters[eventId].webRtcEndpoint.addIceCandidate(candidate);
    }
    else if (type == "viewer") {
        console.info('Sending viewer candidate: sid=' + sessionId + ' eventId=' + eventId);
		for (var i in presenters[eventId]?.eventViewers) {
			var viewer = presenters[eventId]?.eventViewers[i];
			if (viewer.sessionId == sessionId) {
				viewer.webRtcEndpoint.addIceCandidate(candidate);
			}
		}
    }else {
        console.info('Queueing new candidate: sid=' + sessionId + ' eventId=' + eventId);
        if (!candidatesQueue[sessionId]) {
            candidatesQueue[sessionId] = [];
        }
        candidatesQueue[sessionId].push(candidate);
    }
}

function addViewersCount(eventId){
	if (typeof presenters[eventId] !== 'undefined') {
		presenters[eventId].ws.send(JSON.stringify({
			id : 'updateViwersCount',
			viewers: presenters[eventId].eventViewers.length
		}));
		for (var i in presenters[eventId]?.eventViewers) {
			var viewer = presenters[eventId]?.eventViewers[i];
			if(viewer.ws){
				viewer.ws.send(JSON.stringify({
					id : 'updateViwersCount',
					viewers: presenters[eventId].eventViewers.length
				}));
			}
		}
	}
}

function removeViewersCount(eventId){
	if (typeof presenters[eventId] !== 'undefined' && presenters[eventId] !== null) {
		presenters[eventId].ws.send(JSON.stringify({
			id : 'updateViwersCount',
			viewers: presenters[eventId].eventViewers.length
		}));
		for (var i in presenters[eventId]?.eventViewers) {
			var viewer = presenters[eventId]?.eventViewers[i];
			 if(viewer.ws){
				viewer.ws.send(JSON.stringify({
					id : 'updateViwersCount',
					viewers: presenters[eventId].eventViewers.length
				}));
			}
		}
	}
}

app.use(express.static(path.join(__dirname, 'static')));